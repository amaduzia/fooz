<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	wp_enqueue_script( 'script-assets', get_stylesheet_directory_uri() . '/assets/js/script.js', array ( 'jquery' ), 1.1, true);
}

function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Book', 'Post Type General Name', 'twentytwenty-child' ),
		'singular_name'         => _x( 'Book', 'Post Type Singular Name', 'twentytwenty-child' ),
		'menu_name'             => __( 'Books', 'twentytwenty-child' ),
		'name_admin_bar'        => __( 'Book', 'twentytwenty-child' ),
		'archives'              => __( 'Item Archives', 'twentytwenty-child' ),
		'attributes'            => __( 'Item Attributes', 'twentytwenty-child' ),
		'parent_item_colon'     => __( 'Parent Item:', 'twentytwenty-child' ),
		'all_items'             => __( 'All Items', 'twentytwenty-child' ),
		'add_new_item'          => __( 'Add New Item', 'twentytwenty-child' ),
		'add_new'               => __( 'Add New', 'twentytwenty-child' ),
		'new_item'              => __( 'New Item', 'twentytwenty-child' ),
		'edit_item'             => __( 'Edit Item', 'twentytwenty-child' ),
		'update_item'           => __( 'Update Item', 'twentytwenty-child' ),
		'view_item'             => __( 'View Item', 'twentytwenty-child' ),
		'view_items'            => __( 'View Items', 'twentytwenty-child' ),
		'search_items'          => __( 'Search Item', 'twentytwenty-child' ),
		'not_found'             => __( 'Not found', 'twentytwenty-child' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'twentytwenty-child' ),
		'featured_image'        => __( 'Featured Image', 'twentytwenty-child' ),
		'set_featured_image'    => __( 'Set featured image', 'twentytwenty-child' ),
		'remove_featured_image' => __( 'Remove featured image', 'twentytwenty-child' ),
		'use_featured_image'    => __( 'Use as featured image', 'twentytwenty-child' ),
		'insert_into_item'      => __( 'Insert into item', 'twentytwenty-child' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'twentytwenty-child' ),
		'items_list'            => __( 'Items list', 'twentytwenty-child' ),
		'items_list_navigation' => __( 'Items list navigation', 'twentytwenty-child' ),
		'filter_items_list'     => __( 'Filter items list', 'twentytwenty-child' ),
	);
	$args = array(
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor','thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'slug'                  => 'library',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'book', $args );

}
add_action( 'init', 'custom_post_type', 0 );

// Register Custom Taxonomy
function custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Genres', 'Taxonomy General Name', 'twentytwenty-child' ),
		'singular_name'              => _x( 'Genre', 'Taxonomy Singular Name', 'twentytwenty-child' ),
		'menu_name'                  => __( 'Genre', 'twentytwenty-child' ),
		'all_items'                  => __( 'All Items', 'twentytwenty-child' ),
		'parent_item'                => __( 'Parent Item', 'twentytwenty-child' ),
		'parent_item_colon'          => __( 'Parent Item:', 'twentytwenty-child' ),
		'new_item_name'              => __( 'New Item Name', 'twentytwenty-child' ),
		'add_new_item'               => __( 'Add New Item', 'twentytwenty-child' ),
		'edit_item'                  => __( 'Edit Item', 'twentytwenty-child' ),
		'update_item'                => __( 'Update Item', 'twentytwenty-child' ),
		'view_item'                  => __( 'View Item', 'twentytwenty-child' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'twentytwenty-child' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'twentytwenty-child' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'twentytwenty-child' ),
		'popular_items'              => __( 'Popular Items', 'twentytwenty-child' ),
		'search_items'               => __( 'Search Items', 'twentytwenty-child' ),
		'not_found'                  => __( 'Not Found', 'twentytwenty-child' ),
		'no_terms'                   => __( 'No items', 'twentytwenty-child' ),
		'items_list'                 => __( 'Items list', 'twentytwenty-child' ),
		'items_list_navigation'      => __( 'Items list navigation', 'twentytwenty-child' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'genre', array( 'book' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );

add_filter('pre_get_posts', 'change_genre_posts_number');

function change_genre_posts_number($query){
	if ($query->is_tax) {
		if (is_tax('genre')){
		$query->set('posts_per_page', 5);
	}

	}
	return $query;

}

function most_recent_book_func( $atts = 0 ) {
	$args = array(
		'numberposts' => 1,
		'post_type'   => 'book',
		'post_status' => 'publish'
	);

	$recent = get_posts( $args );
	if (!empty($recent[0])) return get_the_title($recent[0]);

}
add_shortcode( 'most_recent_book', 'most_recent_book_func' );

function books_by_genre_func( $atts = 0 ) {

	$args = array(
		'numberposts' => 5,
		'post_type'   => 'book',
		'post_status' => 'publish',
		'orderby'    => 'title',
		'order'       => 'ASC',
		'tax_query'   => array(
			array(
				'taxonomy' => 'genre',
				'field'    => 'term_id',
				'terms'    => $atts['id'],
			)
		)
	);

	if ( ! empty( $atts['id'] ) ) {
		$genre = get_posts( $args );
	}
	$lists = array();
	foreach ( $genre as $book ) {
		$lists[] = get_the_title( $book );
	}

	return ( implode( ',', $lists ) );
}
add_shortcode( 'books_by_genre', 'books_by_genre_func' );


add_action("wp_ajax_get_20_books", "get_20_books");
add_action("wp_ajax_nopriv_get_20_books", "get_20_books");

function get_20_books() {

	if ( !wp_verify_nonce( $_REQUEST['nonce'], "get_20_books_nonce")) {
		exit("");
	}

	$args = array(
		'numberposts' => 20,
		'post_type'   => 'book',
		'post_status' => 'publish',
	);

	$books = get_posts( $args );

	$result = array();
	foreach ($books as $book){
		$book_terms = get_the_terms( $book->ID, 'genre' );
		$terms_string = join(', ', wp_list_pluck($book_terms, 'name'));

		$result['books'][]= array(
			'name'=>get_the_title($book),
			'date,'=>get_the_date($book),
			'genre,'=>$terms_string,
			'excerpt'=>get_the_excerpt($book),
		);
	}


	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		if (!empty($result)) $result['type']='success';
		$result = json_encode($result);
		echo $result;
	}
	else {
		header("Location: ".$_SERVER["HTTP_REFERER"]);
	}
	die();
}

function json_test() {
	$nonce = wp_create_nonce("get_20_books_nonce");
	echo '<div class="test_json" data-nonce="' . $nonce . '" data-ajaxurl="'.admin_url( 'admin-ajax.php' ).'">Test JSON</div>';
}
add_action( 'wp_footer', 'json_test' );