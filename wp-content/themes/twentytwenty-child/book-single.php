<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div class="book">
    <div class="book__title"><?php echo the_title();?></div>
    <div class="book__image"><?php echo the_title();?></div>
    <div class="book__date"><?php echo get_the_date();?></div>
</div>
<?php get_footer(); ?>