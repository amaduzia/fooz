jQuery(document).ready( function() {

    jQuery(".test_json").click( function(e) {
        e.preventDefault();
        nonce = jQuery(this).attr("data-nonce");
        ajaxurl = jQuery(this).attr("data-ajaxurl");

        jQuery.ajax({
            type : "post",
            dataType : "json",
            url : ajaxurl,
            data : {action: "get_20_books",  nonce: nonce},

            success: function(response) {
                if(response.type == "success") {
                   console.log(response);
                }
                else {
                    //alert("Error!")
                    console.log(response);
                }
            }
        })

    })

});